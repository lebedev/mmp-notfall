using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliceScript: MonoBehaviour {
  public AudioSource src;
  public AudioClip jumpSound;
  public Rigidbody2D myRigidbody;
  public LogicScript logic;
  public float jumpStrength;
  public Vector2 velocity;
  public Vector2 acceleration;
  public float jumpAccel;
  public float slowdown;
  public bool jump;
  public float currentJumpCooldown = 0f;

  // Start is called before the first frame update
  void Start() {
    src.clip = jumpSound;
  }

  // Update is called once per frame
  void Update() {

    acceleration += Vector2.right * Input.GetAxis("Horizontal");
    acceleration = acceleration * slowdown;

    velocity = (velocity * slowdown);

    if (transform.position.y < -10f) {
      logic.gameOver();
    }

    Vector2 fwd = transform.TransformPoint(0, -0.03f, 0);

    if (currentJumpCooldown == 0 && Input.GetKeyDown("up")) {
      jump = true;
      jumpAccel = 0.2f;
      currentJumpCooldown = 2f;
      src.Play();
    }
    if (jump || (0.1f < jumpAccel && jumpAccel < 1f)) {
      jumpAccel += 0.05f;
      acceleration += Vector2.up * jumpStrength * jumpAccel;
      jump = false;

    }
    if (currentJumpCooldown > 1f) {
      currentJumpCooldown -= Time.deltaTime;
    } else {
      currentJumpCooldown = 0f;
    }
    velocity = velocity + (acceleration * Time.deltaTime);
    transform.Translate(velocity, 0);
  }

}