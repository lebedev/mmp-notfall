using UnityEngine;
using System.Collections;

public class SmoothFollow2D : MonoBehaviour {
	public Transform target;
	public float distance = 3.0f;
    public float height = 3.0f;
	public float damping = 5.0f;
	public bool smoothRotation = true;
	public bool Rotation = true;
	public bool followBehind = true;
	public bool lockHeight = true;
	public float rotationDamping = 10.0f;

	void Update () {
		Vector2 wantedPosition;
		if(followBehind)
			wantedPosition = target.TransformPoint(0, height, -distance);
		else
			wantedPosition = target.TransformPoint(0, height, distance);
		if(lockHeight)
			transform.position = Vector2.Lerp (transform.position, new Vector2 (wantedPosition.x, transform.position.y) , Time.deltaTime * damping);
		else
			transform.position = Vector2.Lerp (transform.position, wantedPosition, Time.deltaTime * damping);
		
		if (smoothRotation) {
			Quaternion wantedRotation = Quaternion.LookRotation(target.position - transform.position, target.up);
			transform.rotation = Quaternion.Slerp (transform.rotation, wantedRotation, Time.deltaTime * rotationDamping);
		}
		else if (Rotation) {
			transform.LookAt (target, target.up);
		}
	}
}