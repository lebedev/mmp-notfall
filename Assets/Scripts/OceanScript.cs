using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OceanScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
         StartCoroutine(waiter());
    }

    // Update is called once per frame
    void Update()
    {

    }
	IEnumerator waiter()
	{
	while(true)
	{
	 transform.position = Vector2.Lerp (transform.position, Random.insideUnitSphere * 0.1f , Time.deltaTime);
	 yield return new WaitForSecondsRealtime(Random.value);
 
	 }
	 }
}
