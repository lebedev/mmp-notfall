using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SecondLevel : MonoBehaviour
{
    public void LoadNextLevel()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public GameObject gameOverScreen;

    public void LoadScene4()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void OnQuitButton()
    {
        Application.Quit();
    }
    public void gameOver()
    {
        gameOverScreen.SetActive(true);
    }
}