using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FollowButton : MonoBehaviour
{
    public void LoadScene2()
    {
        SceneManager.LoadScene("FallingAnimationScene");
    }
    public void OnQuitButton()
    {
        Application.Quit();
    }
}
