using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonVisibility : MonoBehaviour
{
    public Transform AliceAnimierbar;
    public float activationDistance = 10f;

    void Update()
    {
        // Calculate the distance between the button and the player character
        float distance = Vector3.Distance(transform.position, AliceAnimierbar.position);

        // Enable or disable the button based on the distance
        if (distance <= activationDistance)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}



