using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntoTheRabbitHole : MonoBehaviour
{
    public void LoadScene4()
    {
        SceneManager.LoadScene("TearScene");
    }
    public void OnQuitButton()
    {
        Application.Quit();
    }

}
